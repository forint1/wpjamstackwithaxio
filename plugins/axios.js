import ax from 'axios'

// insert all your axios logic here

export const axios = ax

export default {
  install (Vue, options) {
    Vue.prototype.$axios = ax
  },
  dynamicRoutes(){
    return this.$axios
      .get("https://css-tricks.com/wp-json/wp/v2/posts?page=1&per_page=20")
      .then(res => {
        return res.data.map(post => `/blog/${post.slug}`)
      })
  }
}
