module.exports = function(axios){

  return {
    mode: 'universal',
    /*
    ** Headers of the page
    */
    head: {
      title: process.env.npm_package_name || '',
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
      ],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
      ]
    },
    /*
    ** Customize the progress-bar color
    */
    loading: { color: '#fff' },
    /*
    ** Global CSS
    */
    css: [
      'element-ui/lib/theme-chalk/index.css'
    ],
    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
      '@/plugins/element-ui'
    ],
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [
    ],
    /*
    ** Nuxt.js modules
    */
    modules: [
      '@nuxtjs/axios'
    ],
    axios: {
      proxy: true
    },
    /*
    ** Build configuration
    */
    server: {
      port: 3000, // default: 3000
      host: '127.0.0.1' // default: localhost
    },
    build: {
      transpile: [/^element-ui/],
      /*
      ** You can extend webpack config here
      */
      extend (config, ctx) {
      }
    },
    generate: {
      routes: axios.dynamicRoutes()
    }
  }
}
